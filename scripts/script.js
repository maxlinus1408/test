let answers = [];
const result = document.getElementById('result');
const imageResult = document.getElementById('image-result');

const newYork = 'New-York';
const houston = 'Houston';
const losAngeles = 'Los-angeles';
const maiami = 'Maiami';
const oklahomaCity = 'Oklahoma City'
const augusta = 'Augusta';
const boise = 'Boise';
const chicago = 'Chicago';

const countEntry = (arr) => {
  const counts = {};
  arr.forEach((x) => {
    counts[x] = (counts[x] || 0) + 1;
  });
  return counts;
}

function biggest(obj) {
  let max = 0;
  let emotion; 

  Object.keys(obj).forEach(function(e){
    if(max<Number(obj[e])) { max = Number(obj[e]); emotion = e; }
  });

  return emotion;
}

const outputResult = (str) => {
  if(str === 'New-York') {
    result.innerHTML = 'Ваш результат Нью-Йорк. Огромный шумный современный мегаполис с яркой жизнь жизнью в котором всё есть.';
    imageResult.setAttribute('src' , "./images/new-york.jpg")
  } else if(str === 'Houston') {
    result.innerHTML = 'Ваш результат Хьюстон. Хьюстон  — четвёртый по количеству жителей город в Соединённых Штатах Америки и крупнейший город в штате Техас. Большой и спокойный город с небольшой преступностью. Есть куда сходить.';
    imageResult.setAttribute('src' , "./images/houston.jpg")
  } else if(str === 'Los-angeles') {
    result.innerHTML = 'Ваш результат Лос-Анджелес. Большой город с огромной преступностью и бедностью. Ограблеление считается чем-то обычным. Так же крайне высокие налоги. Люди бегут оттуда.';
    imageResult.setAttribute('src' , "./images/los-angeles.jpg")
  } else if(str === 'Maiami') {
    result.innerHTML = 'Ваш результат Майами. Яркий, солнечный Американский город. Относительно спокойный, но с огромным количеством мест куда можно сходить.';
    imageResult.setAttribute('src' , "./images/maiami.jpg")
  } else if(str === 'Oklahoma City') {
    result.innerHTML = 'Ваш результат  Оклахома-Сити. Оклахома-Сити  — крупнейший город и столица штата Оклахома, административный центр округа Оклахома. Население 650 тыс  жителей, Один из самых благополучных городов США. Единственный минус, не слишком богатый.';
    imageResult.setAttribute('src' , "./images/oklahoma.jpg")
  } else if(str === 'Augusta') {
    result.innerHTML = 'Ваш результат  Огаста (Мэн). Огаста (Мэн) Маленький городок с населением около 18 тысячь  на северо-востоке США, столица штата Мэн и округа Кеннебек. Тихий, спокойный и уютный городок.';
    imageResult.setAttribute('src' , "./images/augusta.jpg")
  } else if(str === 'Boise') {
    result.innerHTML = 'Ваш результат  Бойсе. Потрясающая природа — самая большая притягательная сила в Бойсе, штат Айдахо. Население города 600 тысячь человек. Заснеженные Скалистые горы, создающие захватывающий дух фон, река Бойсе с ее набережными, пересекающая весь город, город, а также множество развлечений на свежем воздухе, таких как катание на лыжах, рыбалка или ходьба на снегоступах, привлекают сюда множество туристов.';
    imageResult.setAttribute('src' , "./images/boyse.jpg")
  } else if(str === 'Chicago') {
    result.innerHTML = 'Ваш результат Чикаго. Сочувствую. Город бьёт рекорды по преступности. Огромные налоги.';
    imageResult.setAttribute('src' , "./images/chicago.jpg")
  }
}

document.getElementById('card1').addEventListener('click', event => { 
  if (event.target.className === 'card1-answer-1') {
    answers.push(houston, losAngeles, maiami);
    document.getElementById('card1').hidden = true;
    document.getElementById('card2').hidden = false;
  } else if(event.target.className === 'card1-answer-2') {
    answers.push(oklahomaCity);
    document.getElementById('card1').hidden = true;
    document.getElementById('card2').hidden = false;
  } else if(event.target.className === 'card1-answer-3') {
    answers.push(newYork, boise, chicago);
    document.getElementById('card1').hidden = true;
    document.getElementById('card2').hidden = false;
  } else if(event.target.className === 'card1-answer-4') {
    answers.push(augusta);
    document.getElementById('card1').hidden = true;
    document.getElementById('card2').hidden = false;
  }
});

document.getElementById('card2').addEventListener('click', event => { 
  if (event.target.className === 'card2-answer-1') {
    answers.push(newYork, losAngeles, maiami);
    document.getElementById('card2').hidden = true;
    document.getElementById('card3').hidden = false;
  } else if(event.target.className === 'card2-answer-2') {
    answers.push(houston, augusta);
    document.getElementById('card2').hidden = true;
    document.getElementById('card3').hidden = false;
  } else if(event.target.className === 'card2-answer-3') {
    answers.push(oklahomaCity,chicago );
    document.getElementById('card2').hidden = true;
    document.getElementById('card3').hidden = false;
  } else if(event.target.className === 'card2-answer-4') {
    answers.push(boise);
    document.getElementById('card2').hidden = true;
    document.getElementById('card3').hidden = false;
  }
});

document.getElementById('card3').addEventListener('click', event => { 
  if (event.target.className === 'card3-answer-1') {
    answers.push(losAngeles, boise);
    document.getElementById('card3').hidden = true;
    document.getElementById('card4').hidden = false;
  } else if(event.target.className === 'card3-answer-2') {
    answers.push(newYork, maiami, augusta, chicago);
    document.getElementById('card3').hidden = true;
    document.getElementById('card4').hidden = false;
  } else if(event.target.className === 'card3-answer-3') {
    answers.push(oklahomaCity, houston);
    document.getElementById('card3').hidden = true;
    document.getElementById('card4').hidden = false;
  } 
});

document.getElementById('card4').addEventListener('click', event => { 
  if (event.target.className === 'card4-answer-1') {
    answers.push(newYork, houston, losAngeles, chicago);
    document.getElementById('card4').hidden = true;
    document.getElementById('card5').hidden = false;
  } else if(event.target.className === 'card4-answer-2') {
    answers.push(maiami, oklahomaCity);
    document.getElementById('card4').hidden = true;
    document.getElementById('card5').hidden = false;
  } else if(event.target.className === 'card4-answer-3') {
    answers.push(boise);
    document.getElementById('card4').hidden = true;
    document.getElementById('card5').hidden = false;
  } else if(event.target.className === 'card4-answer-4') {
    answers.push(augusta);
    document.getElementById('card4').hidden = true;
    document.getElementById('card5').hidden = false;
  }
});

document.getElementById('card5').addEventListener('click', event => { 
  if (event.target.className === 'card5-answer-1') {
    answers.push(newYork, augusta, boise, chicago);
    document.getElementById('card5').hidden = true;
    document.getElementById('card6').hidden = false;
  } else if(event.target.className === 'card5-answer-2') {
    answers.push(houston, maiami, oklahomaCity);
    document.getElementById('card5').hidden = true;
    document.getElementById('card6').hidden = false;
  } else if(event.target.className === 'card5-answer-3') {
    answers.push(losAngeles);
    document.getElementById('card5').hidden = true;
    document.getElementById('card6').hidden = false;
  } else if(event.target.className === 'card5-answer-4') {
    document.getElementById('card5').hidden = true;
    document.getElementById('card6').hidden = false;
  }
});

document.getElementById('card6').addEventListener('click', event => { 
  if (event.target.className === 'card6-answer-1') {
    answers.push(newYork, losAngeles, maiami, chicago);
    document.getElementById('card6').hidden = true;
    document.getElementById('card7').hidden = false;
  } else if(event.target.className === 'card6-answer-2') {
    answers.push(houston, oklahomaCity);
    document.getElementById('card6').hidden = true;
    document.getElementById('card7').hidden = false;
  } else if(event.target.className === 'card6-answer-3') {
    answers.push(boise);
    document.getElementById('card6').hidden = true;
    document.getElementById('card7').hidden = false;
  } else if(event.target.className === 'card6-answer-4') {
    answers.push(augusta);
    document.getElementById('card6').hidden = true;
    document.getElementById('card7').hidden = false;
  }
});

document.getElementById('card7').addEventListener('click', event => { 
  if (event.target.className === 'card7-answer-1') {
    answers.push(losAngeles, chicago);
    document.getElementById('card7').hidden = true;
    document.getElementById('card8').hidden = false;
  } else if(event.target.className === 'card7-answer-2') {
    answers.push(newYork, augusta);
    document.getElementById('card7').hidden = true;
    document.getElementById('card8').hidden = false;
  } else if(event.target.className === 'card7-answer-3') {
    answers.push(houston, maiami, boise);
    document.getElementById('card7').hidden = true;
    document.getElementById('card8').hidden = false;
  } else if(event.target.className === 'card7-answer-4') {
    answers.push(oklahomaCity);
    document.getElementById('card7').hidden = true;
    document.getElementById('card8').hidden = false;
  }
});

document.getElementById('card8').addEventListener('click', event => { 
  if (event.target.className === 'card8-answer-1') {
    answers.push(houston, oklahomaCity, augusta);
    let objectNumberEntry = countEntry(answers);
    const maxEntry = biggest(objectNumberEntry);
    outputResult(maxEntry);
    document.getElementById('card8').hidden = true;
    document.getElementById('card9').hidden = false;
  } else if(event.target.className === 'card8-answer-2') {
    answers.push(maiami, boise);
    let objectNumberEntry = countEntry(answers);
    const maxEntry = biggest(objectNumberEntry);
    outputResult(maxEntry);
    document.getElementById('card8').hidden = true;
    document.getElementById('card9').hidden = false;
  } else if(event.target.className === 'card8-answer-3') {
    answers.push(newYork);
    let objectNumberEntry = countEntry(answers);
    const maxEntry = biggest(objectNumberEntry);
    outputResult(maxEntry);
    document.getElementById('card8').hidden = true;
    document.getElementById('card9').hidden = false;
  } else if(event.target.className === 'card8-answer-4') {
    answers.push(losAngeles, chicago);
    let objectNumberEntry = countEntry(answers);
    const maxEntry = biggest(objectNumberEntry);
    outputResult(maxEntry);
    document.getElementById('card8').hidden = true;
    document.getElementById('card9').hidden = false;
  }
});

